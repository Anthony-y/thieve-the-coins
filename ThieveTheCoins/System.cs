﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThieveTheCoins
{
    public class System
    {
        private string tag;
        private bool isActive = true;

        public System() { }
        public virtual void Update(GameTime gameTime) {  }
        public virtual void Draw(SpriteBatch spriteBatch) {  }
        public string GetTag() { return tag; }
        public bool GetActive() { return isActive; }
        public void SetActive(bool newActive) { isActive = newActive; }
        public void SetTag(string tag) { this.tag = tag; }
    }
}
