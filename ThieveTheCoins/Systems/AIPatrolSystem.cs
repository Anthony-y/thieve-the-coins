﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ThieveTheCoins.Components;

namespace ThieveTheCoins.Systems
{
    class AIPatrolSystem : System
    {
        private EntityManager enityManager;
        private CAIPatrol aiPatrol;
        private Game1 game;

        public AIPatrolSystem(EntityManager enityManager, CAIPatrol aiPatrol, Game1 game)
        {
            SetTag("AIPatrolSystem");

            this.game = game;
            this.enityManager = enityManager;
            this.aiPatrol = aiPatrol;
        }

        public override void Update(GameTime gameTime)
        {
            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent("CCollider").GetCollider().
                Intersects(enityManager.GetEntityFromTag("Player").GetComponent("CCollider").GetCollider()))
            {
                MessageBox.Show("You lose!");
                game.Exit();

                enityManager.GetEntityFromTag("Player").GetComponent("CTransform").SetPositionVector(
                new Vector2(50, 50));
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent("CTransform").
                GetPositionVector().X <= 0 && aiPatrol.GetPatrolType() == true)
            {
                aiPatrol.SetDirection(EDirection.right);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent("CTransform").
                GetPositionVector().X >= 640 - 32 && aiPatrol.GetPatrolType() == true)
            {
                aiPatrol.SetDirection(EDirection.left);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent("CTransform").
                GetPositionVector().Y <= 0 && aiPatrol.GetPatrolType() == false)
            {
                aiPatrol.SetDirection(EDirection.down);
            }

            if (enityManager.GetEntityFromTag(aiPatrol.GetOwner().GetTag()).GetComponent("CTransform").
                GetPositionVector().Y >= 480 - 32 && aiPatrol.GetPatrolType() == false)
            {
                aiPatrol.SetDirection(EDirection.up);
            }

            base.Update(gameTime);
        }
    }
}
