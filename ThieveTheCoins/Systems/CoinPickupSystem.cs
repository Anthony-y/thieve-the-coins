﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ThieveTheCoins.Components;
using ThieveTheCoins.Managers;

namespace ThieveTheCoins.Systems
{
    public class CoinPickupSystem : System
    {
        private CCoinPickup coinPickup;
        private EntityManager entityManager;
        private SystemsManager systemsManager;
        private Random rand;
        private Game1 game;

        private SoundEffect youWonSound;
        private SoundEffect coinPickupSound;

        private SpriteFont font;

        private int winningScore = 5;
        private bool hasWon = false;
        private bool isMuted = false;

        public CoinPickupSystem(CCoinPickup coinPickup, EntityManager entityManager, SystemsManager systemsManager,
            Game1 game, ContentManager content)
        {
            SetTag("CoinPickupSystem");

            this.game = game;
            this.entityManager = entityManager;
            this.systemsManager = systemsManager;
            this.coinPickup = coinPickup;
            rand = new Random();

            font = content.Load<SpriteFont>("Arial");
            youWonSound = content.Load<SoundEffect>("You Won!");
            coinPickupSound = content.Load<SoundEffect>("CoinPickup");
        }

        public override void Update(GameTime gameTime)
        {
            if (coinPickup.GetScore() >= winningScore)
            {
                try { if (!isMuted) { youWonSound.Play(); } }
                catch { }
                MessageBox.Show("You won!");
                game.Exit();
            }

            if (!hasWon && entityManager.GetEntityFromTag(coinPickup.GetOwner().GetTag()).GetComponent("CCollider").GetCollider().Intersects(
                entityManager.GetEntityFromTag("Coin").GetComponent("CCollider").GetCollider()))
            {
                try { coinPickupSound.Play(); }
                catch { }
                coinPickup.AddScore(1);
                Console.WriteLine(coinPickup.GetScore());
                entityManager.GetEntityFromTag("Coin").GetComponent("CTransform").SetPositionVector(
                    new Vector2(rand.Next(100, 301), rand.Next(100, 301)));
            }
            
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(font, "Score: " + coinPickup.GetScore() + "/" + winningScore, new Vector2(640 / 2 - 32, 7), Color.White);

            base.Draw(spriteBatch);
        }
    }
}
