﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThieveTheCoins
{
    public class Component
    {
        private string tag;
        private bool isActive = true;
        private Entity owner;

        public Component(Entity owner) { this.owner = owner; }
        public virtual void Update(GameTime gameTime) { if (!isActive) { return; } }
        public virtual void Draw(SpriteBatch spriteBatch) { if (!isActive) { return; } }
        public void SetTag(string tag) { this.tag = tag; }
        public string GetTag() { return tag; }
        public bool GetActive() { return isActive; }
        public Entity GetOwner() { return owner; }
    }
}
