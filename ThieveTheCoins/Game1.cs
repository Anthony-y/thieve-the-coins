﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using ThieveTheCoins.Managers;
using ThieveTheCoins.Components;
using ThieveTheCoins.Systems;
using System.IO;
using System.Reflection;

namespace ThieveTheCoins
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont font;

        public EntityManager entityManager;
        public SystemsManager systemsManager;

        public dynamic newEntityTable;
        public dynamic compTable;
        public string[] files;
        public List<NLua.LuaTable> tables;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            entityManager = new EntityManager();
            systemsManager = new SystemsManager();

            graphics.PreferredBackBufferWidth = 640;
            graphics.PreferredBackBufferHeight = 480;

            IsMouseVisible = true;
            Content.RootDirectory = "Content";
            Window.AllowUserResizing = false;
        }

        protected override void Initialize()
        {
            ScriptManager.Initialize();

            ScriptManager.RegisterFunction("gimme", "GetEntityFromTag", entityManager);
            ScriptManager.RegisterFunction("gimmec", "GetComponent", entityManager);

            ScriptManager.RegisterVariable("entityManager", entityManager);
            ScriptManager.RegisterVariable("systemsManager", systemsManager);

            // Get all scripts in Scripts directory. //
            files = Directory.GetFiles
            (
                "Scripts",
                "*.lua", SearchOption.TopDirectoryOnly
            );

            // Loop through files in Scripts directory and run them.
            foreach (var file in files) { ScriptManager.GetLua().DoFile(file); }

            ScriptManager.InitScripts();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            font = Content.Load<SpriteFont>("Arial");

            //                              <ENTITY CONSTRUCTORS>                                       //
            //------------------------------------------------------------------------------------------//
            //                          Build entities from Lua scripts,                                //
            //                     Adds component to entity if entity already exists.                   //

            AddLuaEntity("Player", "Transform");            AddLuaEntity("Follower", "Transform");
            AddLuaEntity("Player", "Texture");              AddLuaEntity("Follower", "Texture");
            AddLuaEntity("Player", "BorderCollision");      AddLuaEntity("Follower", "BorderCollision");
            AddLuaEntity("Player", "Collider");             AddLuaEntity("Follower", "Collider");
            AddLuaEntity("Player", "MovementControl");      AddLuaEntity("Follower", "MovementControl");
            AddLuaEntity("Player", "CoinPickup");
            //-----------------------------------------------------------------------------------------//

            AddLuaEntity("EnemyX", "Transform");            AddLuaEntity("EnemyY", "Transform");
            AddLuaEntity("EnemyX", "Texture");              AddLuaEntity("EnemyY", "Transform");
            AddLuaEntity("EnemyX", "BorderCollision");      AddLuaEntity("EnemyY", "Texture");
            AddLuaEntity("EnemyX", "Collider");             AddLuaEntity("EnemyY", "BorderCollision");
            AddLuaEntity("EnemyX", "AIPatrol");             AddLuaEntity("EnemyY", "Collider");
                                                            AddLuaEntity("EnemyY", "Collider");
                                                            AddLuaEntity("EnemyY", "AIPatrol");
            //-----------------------------------------------------------------------------------------//

            AddLuaEntity("Coin", "Transform");              
            AddLuaEntity("Coin", "Texture");                
            AddLuaEntity("Coin", "Collider");
            AddLuaEntity("Coin", "BorderCollision");

            //------------------------------------------------------------------------------------------//
            //                              </ENTITY CONSTRUCTORS>                                      //
            //                          Build entities from Lua scripts.                                //

            base.LoadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            entityManager.UpdateEntities(gameTime);
            systemsManager.UpdateSystems(gameTime);
            ScriptManager.Update(gameTime);

            if (Keyboard.GetState().IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Escape)) { Exit(); }

            // "Follower" might not exist so we have to check for it. //
            if (entityManager.DoesEntityExist("Follower")) {
                if (entityManager.GetEntityFromTag("Player").GetComponent("CCollider").GetCollider().Intersects(
                    entityManager.GetEntityFromTag("Follower").GetComponent("CCollider").GetCollider()))
                {
                    MessageBox.Show("You lose!");
                    Exit();
                }
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Coral);

            // Scripts
            spriteBatch.Begin();
            ScriptManager.Draw(spriteBatch);
            spriteBatch.End();

            // Entities
            spriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend);
            entityManager.DrawEntities(spriteBatch);
            systemsManager.DrawSystems(spriteBatch);
            spriteBatch.End();

            // GUI/Font
            spriteBatch.Begin();
            spriteBatch.DrawString(font, "By Anthony Baynham 2015\nv 2.1",
                new Vector2(5, 7), Color.White);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        // Component template: //
        // entityManager.AddComponent(newEntityTable["name"], new CTransform(entityManager.GetEntityFromTag(newEntityTable["name"]), //
        public void AddLuaEntity(string pathOrTable, string componentName)
        {
            newEntityTable = ScriptManager.GetTable(pathOrTable);
            compTable = ScriptManager.GetTable(pathOrTable + "." + componentName);

            // If the entity already exists, don't create it again. //
            if (!entityManager.DoesEntityExist(newEntityTable["name"]))
            {
                entityManager.SetupEntity(newEntityTable["name"]);
                LuaParser(componentName);
            }

            else { LuaParser(componentName); }
        }

        public void LuaParser(string componentName) 
        {
            if (componentName == "CTransform" || componentName == "Transform")
            {
                entityManager.AddComponent(newEntityTable["name"], new CTransform(entityManager.GetEntityFromTag(newEntityTable["name"]),
                    new Vector2((float)compTable["x"], (float)compTable["y"]), new Vector2((float)compTable["scaleX"], (float)compTable["scaleY"])));
            }

            if (componentName == "CCollider" || componentName == "Collider")
            {
                if (!entityManager.GetEntityFromTag(newEntityTable["name"]).DoesComponentExist("CCollider"))
                {
                    entityManager.AddComponent(newEntityTable["name"], new CCollider(entityManager.GetEntityFromTag(newEntityTable["name"])));
                }
            }

            if (componentName == "CTexture" || componentName == "Texture")
            {
                entityManager.AddComponent(newEntityTable["name"], new CTexture(entityManager.GetEntityFromTag(newEntityTable["name"]), Content,
                    compTable["texture"], Color.White));
            }

            if (componentName == "CBorderCollision" || componentName == "BorderCollision")
            {
                entityManager.AddComponent(newEntityTable["name"], new CBorderCollision(entityManager.GetEntityFromTag(newEntityTable["name"])));
            }

            if (componentName == "CAIPatrol" || componentName == "AIPatrol")
            {
                entityManager.AddComponent(newEntityTable["name"], new CAIPatrol(entityManager.GetEntityFromTag(newEntityTable["name"]), compTable["isX"]));
                systemsManager.SetupSystem(new AIPatrolSystem(entityManager, entityManager.GetEntityFromTag(newEntityTable["name"]).GetComponent("CAIPatrol"), 
                    this), newEntityTable["name"] + "AIPatrol");
            }

            if (componentName == "CMovementControl" || componentName == "MovementControl")
            {
                if (compTable["speed"] <= 0)
                {
                    entityManager.AddComponent(newEntityTable["name"], new CMovementControl(entityManager.GetEntityFromTag(newEntityTable["name"])));
                } else
                {
                    entityManager.AddComponent(newEntityTable["name"], new CMovementControl(entityManager.GetEntityFromTag(newEntityTable["name"]), 
                        (float)compTable["speed"]));
                }
            }

            if (componentName == "CCoinPickup" || componentName == "CoinPickup")
            {
                entityManager.AddComponent(newEntityTable["name"], new CCoinPickup(entityManager.GetEntityFromTag(newEntityTable["name"])));
                systemsManager.SetupSystem(new CoinPickupSystem(entityManager.GetEntityFromTag(newEntityTable["name"]).GetComponent("CCoinPickup"),
                    entityManager, systemsManager, this, Content), newEntityTable["name"] + "CoinPickup");
            }
        }
    }
}
