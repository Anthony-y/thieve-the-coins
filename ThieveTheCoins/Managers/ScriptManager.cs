﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using NLua;
using Microsoft.Xna.Framework.Graphics;
using System.Reflection;

namespace ThieveTheCoins.Managers
{
    public static class ScriptManager
    {
        private static string scriptPath;
        private static string scriptType;
        private static List<LuaFunction> scripts;

        private static Lua lua;

        public static void Initialize()
        {
            scripts = new List<LuaFunction>();
            lua = new Lua();
            lua.LoadCLRPackage();
        }
        
        public static Lua GetLua() { return lua; }

        public static LuaTable GetTable(string tableNameInLua)
        {
            var newTable = lua.GetTable(tableNameInLua);
            return newTable;
        }

        public static void RegisterVariable(string name, dynamic value)
        {
            lua[name] = value;
        }

        public static void RegisterFunction(string luaName, string csharpName, dynamic instance)
        {
            lua.RegisterFunction(luaName, instance, instance.GetType().GetMethod(csharpName));
        }

        public static void LoadScript(string path, string type)
        {
            path = "D:\\Magic\\C#\\ThieveTheCoins\\ThieveTheCoins\\Scripts\\" + path + ".lua";
            scriptPath = path;
            scriptType = type;
            var loadedScript = lua.LoadFile(path);
            scripts.Add(loadedScript);
        }

        public static void InitScripts()
        {
            foreach (var s in scripts)
            {
                if (scriptType.ToLower() == "init") { lua.DoFile(scriptPath); }
                else { return; }
            }
        }

        public static void Update(GameTime gameTime)
        {
            foreach (var s in scripts)
            {
                if (scriptType.ToLower() == "update")
                {
                    try
                    {
                        lua.DoFile(scriptPath);
                        LuaFunction update = lua["Update"] as LuaFunction;
                        update.Call();
                    }
                    catch { Console.WriteLine("Something went wrong, make sure you have an Update() function."); }
                }

                else { return; }
            }

        }

        public static void Draw(SpriteBatch spriteBatch)
        {
            foreach (var s in scripts)
            {
                if (scriptType.ToLower() == "draw")
                {
                    try
                    {
                        lua.DoFile(scriptPath);
                        LuaFunction draw = lua["Draw"] as LuaFunction;
                        draw.Call();
                    }
                    catch { Console.WriteLine("Something went wrong, make sure you have a Draw() function."); }
                }

                else { return; }
            }
        }
    }
}
