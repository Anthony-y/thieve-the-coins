﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThieveTheCoins.Components
{
    public class CTexture : Component
    {
        private ContentManager content;
        private CTransform transform;
        private Texture2D texture;
        private Color color;
        private Entity owner;
        private string spriteName;

        public CTexture(Entity owner, ContentManager content, string spriteName, Color color)
            : base(owner)
        {
            SetTag("CTexture");
            this.owner = owner;
            this.content = content;
            this.spriteName = spriteName;
            this.color = color;
            transform = owner.GetComponent("CTransform");
            texture = content.Load<Texture2D>(spriteName);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, transform.GetPositionVector(), color: color, scale: transform.GetScaleVector());

            base.Draw(spriteBatch);
        }

        public Texture2D GetTexture() { return texture; }
        public Color GetColor() { return color; }

    }
}
