﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NLua;
using System.IO;
using Microsoft.Xna.Framework.Graphics;

namespace ThieveTheCoins.Components
{
    public class CScript : Component
    {
        private Entity owner;
        private Lua lua;
        private List<LuaFunction> scripts;
        private LuaTable currentEntTable;
        private LuaFunction luaUpdate;
        private LuaFunction luaDraw;

        public CScript(Entity owner)
            : base(owner)
        {
            lua = new Lua();
            scripts = new List<LuaFunction>();
            this.owner = owner;

            string[] files = Directory.GetFiles("D:\\Magic\\C#\\ThieveTheCoins\\ThieveTheCoins\\Scripts\\",
                "*.lua", SearchOption.TopDirectoryOnly);

            foreach (var file in files)
            {
                var newScript = lua.LoadFile(file);
                scripts.Add(newScript);

                var shortened = file.Substring(0, file.Length - 4);
                currentEntTable = lua.GetTable(shortened);

                try {
                    //luaUpdate = lua.GetFunction(currentEntTable["Update"]);
                    luaDraw = lua.GetFunction(shortened + "Draw");
                } catch { Console.WriteLine("Draw or Update function not found!"); }
                Console.WriteLine(file.ToString());
            }
        }

        public override void Update(GameTime gameTime)
        {
            try {
                luaUpdate.Call();
                Console.WriteLine("Found update function.");
            } catch { }

            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            try {
                luaDraw.Call();
                Console.WriteLine("Found draw function.");
            } catch { }

            base.Draw(spriteBatch);
        }

    }
}
