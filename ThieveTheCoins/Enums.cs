﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ThieveTheCoins
{
    public enum EDirection
    {
        up,
        down,
        left,
        right
    }

    public enum ComponentType
    {
        AIPatrol,
        BorderCollision,
        CoinPickup,
        Collider,
        Quest,
        CTexture,
        Transform,
        WASDControl
    }

}
