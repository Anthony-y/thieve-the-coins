﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

using ThieveTheCoins.Managers;

namespace ThieveTheCoins
{
    public class Entity
    {
        private List<Component> components;
        private string tag;

        public Entity() { components = new List<Component>(); }
        public void AddComponent(Component c) { components.Add(c); }
        public void Update(GameTime gameTime)
        {
            foreach (Component c in components)
            {
                c.Update(gameTime);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (Component c in components)
            {
                c.Draw(spriteBatch);
            }
        }

        public dynamic GetComponent(string componentTag)
        {
            if (DoesComponentExist(componentTag))
            {
                var match = components.FirstOrDefault(com => com.GetTag().Contains(componentTag));
                return match;
            }

            else
            {
                Console.WriteLine("That component doesn't exist on " + tag);
                return null;
            }

        }

        public bool DoesComponentExist(string name)
        {
            if (components.Any(com => com.GetTag() == name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Component> GetAllComponents() { return components; }
        public string GetTag() { return tag; }
        public void SetTag(string tag) { this.tag = tag; }

    }
}
